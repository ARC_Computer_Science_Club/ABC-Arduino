;;;/***********************************************************************************
 *
 * ABCArduino
 * ARC Computer Science Club, Spring 2014
 *
 * version 0.0.0.38
 *
 * Change SERVOSELECT to Purple or Black to choose pen servo.
 * Change ROBOTSELECT to Johnny or Bender to choose robot.
 * For now, can test code hardcodded to Bender
 *
 ******************************************************************************/

#include <ZumoMotors.h>
#include <Pushbutton.h>
#include <stdlib.h>
#include <math.h>

#define MAX_LEFT_MOTOR_BENDER 200 //400
#define MAX_RIGHT_MOTOR_BENDER 174 //325
#define MAX_LEFT_MOTOR_JOHNNY 200 //400
#define MAX_RIGHT_MOTOR_JOHNNY 163 //? 360 //Original value 360
#define MAX_LEFT_MOTOR_BISHOP 200 //Unconfirmed
#define MAX_RIGHT_MOTOR_BISHOP 200 //Unconfirmed
#define MAX_MOTOR_GENERIC 400

#define START_PURPLE_SERVO 450
#define MID_PURPLE_SERVO 1340
#define END_PURPLE_SERVO 2300
#define PEN_DOWN_PURPLE_SERVO 450 //Pen values may need changing depending on how servo is attached.
#define PEN_UP_PURPLE_SERVO 1340

//These values assume black motor has label facing down.
//Attach pen to left blade of motor, looking at blade/usb port/power switch
//Make sure blade is attached at 90 angle, perpendicular (parallel?) to motor case.
#define START_BLACK_SERVO 0
#define MID_BLACK_SERVO 0
#define END_BLACK_SERVO 0
#define PEN_DOWN_BLACK_SERVO 2300//2000//850 //orig 500
#define PEN_UP_BLACK_SERVO 2100//1500//900//2000
#define PEN_DELAY_BLACK_SERVO 2300

#define LED_PIN 13

//For use when setting motor speeds to 0,0. May need adjustment
#define MOTOR_STOP_DELAY 25

//These two values are wrong given fresh batteries
//Assumes 9x9 letter, given traveling at max speed straight.
#define LINE_DELAY_DISTANCE 740//833.5//740 is 8x8 //210 
//Use this value to have a small gap between letters.
#define BORDER_DELAY_DISTANCE 740// 833.5//740 //200

#define TURN_90_DELAY 440 //445 //215

//Three step delay - Go back this ammount, turn, then go forwrad this ammount
#define THREE_STEP_DELAY 210

//The ratio between motors to get 1/4th of a circle drawn.
#define CURVE_MOTOR_RATIO 0.25

enum Direction {Left, Right, UnkownDirection};

enum Robots {Bender, Johnny, Bishop, UnkownRobot};
enum Servos {Purple, Black, UnkownServo};

//Change this line to change robot
Robots ROBOTSELECT = Johnny; //Changed to test drawElipse Bender;
Servos SERVOSELECT = Black;

//Primative function prototypes would go here.
void setPen(bool PenDown);

void drawString(char* strToDraw);

void threePointTurn(Direction d);
void threePointTurn(float offsetFrom90, Direction d);

void travelStraight();
void travelStraight(int leftMotorSpeed, int rightMotorSpeed);
void travelStraight(int leftMotorSpeed, int rightMotorSpeed, unsigned travelDelayMS);
void travelStraight(int leftMotorSpeed, int rightMotorSpeed, unsigned travelDelayMS, unsigned stopDelayMS);

void travelCurve(); //Travel in 1/4th quadrants. From bottom midpoint to right midpoint, or start to center
void travelCurve(double motorRatioOffset);
void travelCurve(double motorRatioOffset, unsigned travelDelayMS);
void travelCurve(double motorRatioOffset, unsigned travelDelayMS, unsigned stopDelayMS);

void drawA();
void drawB();
void drawC();
void drawD();
void drawE();
void drawF();
void drawG();
void drawH();
void drawI();
void drawJ();
void drawK();
void drawL();
void drawM();
void drawN();
void drawO();
void drawP();
void drawQ();
void drawR();
void drawS();
void drawT();
void drawU();
void drawV();
void drawW();
void drawX();
void drawY();
void drawZ();
void drawBox();
void drawSpace();
void drawBackspace();
void newLine();

Pushbutton button(ZUMO_BUTTON);
ZumoMotors motors;

int _MaxLeftMotor = 0;
int _MaxRightMotor = 0;

int _PenDown = 0;
int _PenUp = 0;

int _CurrentLine = 0;
int _LineDistance = 0;

bool _FirstRun = true;

void setup()
{
  Serial.begin(9600);
  
  //set up LED
  pinMode(LED_PIN, OUTPUT);
  
  //Set up servo
  servoInit();
  
  //Set robot
  if(ROBOTSELECT == Bender)
  {
    _MaxLeftMotor = MAX_LEFT_MOTOR_BENDER;
    _MaxRightMotor = MAX_RIGHT_MOTOR_BENDER;
  }
  else if (ROBOTSELECT == Johnny)
  {
    _MaxLeftMotor = MAX_LEFT_MOTOR_JOHNNY;
    _MaxRightMotor = MAX_RIGHT_MOTOR_JOHNNY;
  }
  else if (ROBOTSELECT == Bishop)
  {
    _MaxLeftMotor = MAX_LEFT_MOTOR_BISHOP;
    _MaxRightMotor = MAX_RIGHT_MOTOR_BISHOP;
  }
  else
  {
    _MaxLeftMotor = MAX_MOTOR_GENERIC;
    _MaxRightMotor = MAX_MOTOR_GENERIC;
  }
  
  //Set Servo
  if(SERVOSELECT == Purple)
  {
    _PenDown = PEN_DOWN_PURPLE_SERVO;
    _PenUp = PEN_UP_PURPLE_SERVO;
  }
  else if (SERVOSELECT == Black)
  {
    _PenDown = PEN_DOWN_BLACK_SERVO;
    _PenUp = PEN_UP_BLACK_SERVO;
  }
  else
  {
    _PenDown = PEN_DOWN_BLACK_SERVO;
    _PenUp = PEN_UP_BLACK_SERVO;
  }
  
  Serial.println("Setup complete");
}

void loop()
{
  if(_FirstRun)
  {
    Serial.println("Waiting for button press to begin... ");
    
    //Second button press to actually start the program. 
    while (!button.isPressed());
    
    delay(500);
    setPen(false);
    
    _FirstRun = false;
  }   
  
  Serial.println("Start of loop");  

  //To move pen up or down, setPen(true/false) where true is writing with pen, false is pen lifted.  
  //To test individual letter,
  //drawA();
  //drawCircle();
  
  drawString("Happy Mothers Day");
    
  Serial.println("End of loop. Waiting for button presss");
  
  while(!button.isPressed());
}

//Primative functions would go here.

void setPen(bool PenDown)
{
  if(PenDown)
  {
    servoSetPosition(_PenDown);
    delay(_PenDown);
  }
  else
  {
    servoSetPosition(_PenUp);
    delay(_PenUp);
  }
}

void threePointTurn(Direction d)
{
  threePointTurn(1, d);
}

void travelStraight()
{
  travelStraight(_MaxLeftMotor, _MaxRightMotor, LINE_DELAY_DISTANCE, MOTOR_STOP_DELAY);
}

void travelStraight(int leftMotorSpeed, int rightMotorSpeed)
{
  travelStraight(leftMotorSpeed, rightMotorSpeed, LINE_DELAY_DISTANCE, MOTOR_STOP_DELAY);
}

void travelStraight(int leftMotorSpeed, int rightMotorSpeed, unsigned travelDelayMS)
{
  travelStraight(leftMotorSpeed, rightMotorSpeed, travelDelayMS, MOTOR_STOP_DELAY);
}
void travelStraight(int leftMotorSpeed, int rightMotorSpeed, unsigned travelDelayMS, unsigned stopDelayMS)
{
  motors.setSpeeds(leftMotorSpeed, rightMotorSpeed);
  delay(travelDelayMS);
  motors.setSpeeds(0,0);
  if(stopDelayMS != 0)
    delay(stopDelayMS);
}

void threePointTurn(float offsetFrom90, Direction d)
{  
  float offset_delay = offsetFrom90 * TURN_90_DELAY;
  Serial.print("offsetFrom90 is: ");
  Serial.println(offsetFrom90);
  Serial.print("Offset delay is: ");
  Serial.println(offset_delay);
  
  //Go back a bit
  motors.setSpeeds(-_MaxLeftMotor, -_MaxRightMotor);
  delay(offsetFrom90 * THREE_STEP_DELAY);
  motors.setSpeeds(0,0);
  delay(MOTOR_STOP_DELAY);
  
  //Set direction
  if(d == Left)
    motors.setSpeeds(-_MaxLeftMotor, _MaxRightMotor);
  else if (d == Right)
    motors.setSpeeds(_MaxLeftMotor, -_MaxRightMotor);
    
  delay(offset_delay);
  
  motors.setSpeeds(0,0);
  delay(MOTOR_STOP_DELAY);
  
  //Go forward a bit
  motors.setSpeeds(_MaxLeftMotor, _MaxRightMotor);
  delay(offsetFrom90 * THREE_STEP_DELAY);
  motors.setSpeeds(0,0);
  delay(MOTOR_STOP_DELAY);
}

//START OF LETTERS
void drawA()
{
  //Complete Rewrite. Now actually works, aside from the A ridge not being right.
  //Distance between upper corners and lower center. Borrowed from untested drawY. Fixed to be 8*4 instead of 8*8
  
  double travelDelay = sqrt( pow( LINE_DELAY_DISTANCE/2, 2 ) + pow( LINE_DELAY_DISTANCE, 2 ) );
  threePointTurn(0.5, Left);
  setPen(true);
  travelStraight(_MaxLeftMotor, _MaxRightMotor, travelDelay, 0);
  setPen(false);
  threePointTurn(0.5, Right);
  threePointTurn(0.5, Right);
  setPen(true);
  travelStraight(_MaxLeftMotor, _MaxRightMotor, travelDelay, 0);
  setPen(false);
  
  //Draw middle ridge in A.
  
  travelStraight(-_MaxLeftMotor, -_MaxRightMotor, travelDelay/2, 0);
  threePointTurn(0.5, Left);
  travelStraight(-_MaxLeftMotor, -_MaxRightMotor, LINE_DELAY_DISTANCE/4, 0); //LINE_DELAY/4 is placeholder
  setPen(true);
  travelStraight(_MaxLeftMotor, _MaxRightMotor, LINE_DELAY_DISTANCE/4, 0);
  setPen(false);
  threePointTurn(0.5, Right);
  travelStraight(_MaxLeftMotor, _MaxRightMotor, travelDelay/2, 0); 
  
  //Turn and go to end.
  threePointTurn(0.5, Left);
  return;
}

void drawB()
{
  //UNTESTED
  threePointTurn(Left);
  setPen(true);
  travelStraight();
  setPen(false);
  threePointTurn(Right);
  setPen(true);
  drawCircle(2, Right, 2);
  setPen(false);
  //threePointTurn(2, Right); // Turn 180
  threePointTurn(Right);
  threePointTurn(Right);
  setPen(true);
  drawCircle(2, Right, 2);
  setPen(false);
  threePointTurn(Right);
  threePointTurn(Right);
  travelStraight();
}

void drawC()
{
  //UNTESTED
  travelStraight(_MaxLeftMotor, _MaxRightMotor, LINE_DELAY_DISTANCE/2, 0);
  threePointTurn(Left); //travel 180
  threePointTurn(Left);
  setPen(true);
  drawCircle(2, Right);
  setPen(false);
  threePointTurn(Right);
  travelStraight();
  threePointTurn(Left);
  travelStraight(_MaxLeftMotor, _MaxRightMotor, LINE_DELAY_DISTANCE/2, 0);  
}
void drawD()
{
  //Derived from drawB
  //UNTESTED
  threePointTurn(Left);
  setPen(true);
  travelStraight();
  setPen(false);
  threePointTurn(Right);
  setPen(true);
  drawCircle(2, Right);
  setPen(false);
  threePointTurn(Right); // Turn 180
  threePointTurn(Right);
  travelStraight();
}

void drawE()
{
  //Turn left
  threePointTurn (Left);

  setPen(true);
  
  //Go to border
  motors.setSpeeds(_MaxLeftMotor, _MaxRightMotor);
  delay(LINE_DELAY_DISTANCE);
  motors.setSpeeds(0,0);
  
  setPen(false);
  
  threePointTurn(Right);
  
  setPen(true);
  
  //go to other side
  motors.setSpeeds(_MaxLeftMotor, _MaxRightMotor);
  delay(LINE_DELAY_DISTANCE);
  motors.setSpeeds(0,0);
  
  setPen(false);
  
  //Go back
  motors.setSpeeds(-_MaxLeftMotor, -_MaxLeftMotor);
  delay(LINE_DELAY_DISTANCE);
  motors.setSpeeds(0,0);
  delay(MOTOR_STOP_DELAY);
  
  //turn left
  threePointTurn(Left);
    
  //Go back half way
  motors.setSpeeds(-_MaxLeftMotor, -_MaxRightMotor);
  delay(LINE_DELAY_DISTANCE / 2);
  motors.setSpeeds(0,0);
  delay(MOTOR_STOP_DELAY);
  
  
  //Turn right
  threePointTurn(Right);
  
  setPen(true);
  
  //Go across
  motors.setSpeeds(_MaxLeftMotor,_MaxRightMotor);
  delay(LINE_DELAY_DISTANCE);
  motors.setSpeeds(0,0);
  
  setPen(false);
  
  //Go back
  motors.setSpeeds(-_MaxLeftMotor, -_MaxLeftMotor);
  delay(LINE_DELAY_DISTANCE);
  motors.setSpeeds(0,0);
  delay(MOTOR_STOP_DELAY);
  
  //turn left
  threePointTurn(Left);
  
  //Go back half way
  motors.setSpeeds(-_MaxLeftMotor, -_MaxRightMotor);
  delay(LINE_DELAY_DISTANCE / 2);
  motors.setSpeeds(0,0);
  delay(MOTOR_STOP_DELAY);
  
  
  //Turn right
  threePointTurn(Right);
  
  setPen(true);
  
  //Go across
  motors.setSpeeds(_MaxLeftMotor,_MaxRightMotor);
  delay(LINE_DELAY_DISTANCE);
  motors.setSpeeds(0,0);
  
  setPen(false);
}

void drawF()
{
  //Turn left
  threePointTurn(Left);

  setPen(true);
  
  //Go to border
  motors.setSpeeds(_MaxLeftMotor, _MaxRightMotor);
  delay(LINE_DELAY_DISTANCE);
  motors.setSpeeds(0,0);
  
  setPen(false);
  
  //Turn right
  threePointTurn(Right);
  
  setPen(true);
  
  //go to other side
  motors.setSpeeds(_MaxLeftMotor, _MaxRightMotor);
  delay(LINE_DELAY_DISTANCE);
  motors.setSpeeds(0,0);
  
  setPen(false);
  
  //Go back
  motors.setSpeeds(-_MaxLeftMotor, -_MaxLeftMotor);
  delay(LINE_DELAY_DISTANCE);
  motors.setSpeeds(0,0);
  delay(MOTOR_STOP_DELAY);
  
  //turn left
  threePointTurn(Left);
  
  //Go back half way
  motors.setSpeeds(-_MaxLeftMotor, -_MaxRightMotor);
  delay(LINE_DELAY_DISTANCE / 2);
  motors.setSpeeds(0,0);
  delay(MOTOR_STOP_DELAY);
  
  
  //Turn right
  threePointTurn(Right);
  
  setPen(true);
  
  //Go across
  motors.setSpeeds(_MaxLeftMotor,_MaxRightMotor);
  delay(LINE_DELAY_DISTANCE);
  motors.setSpeeds(0,0);
  
  setPen(false);
  
  //Turn right
  threePointTurn(Right);
  
  //go to lower right corner
  motors.setSpeeds(_MaxLeftMotor, _MaxRightMotor);
  delay(LINE_DELAY_DISTANCE/2);
  motors.setSpeeds(0,0);
  delay(MOTOR_STOP_DELAY);
  
  //Turn left
  threePointTurn(Left);
}

void drawG()
{
  //UNTESTED
  travelStraight(_MaxLeftMotor, _MaxRightMotor, LINE_DELAY_DISTANCE/2, MOTOR_STOP_DELAY);
  threePointTurn(Left);
  travelStraight();
  threePointTurn(Left);
  setPen(true);
  drawCircle(2, Left);
  drawCircle(4, Left);
  setPen(false);
  threePointTurn(Left);
  setPen(true);
  travelStraight(_MaxLeftMotor, _MaxRightMotor, LINE_DELAY_DISTANCE/2, 0);
  setPen(false);
  threePointTurn(Left);
  travelStraight(_MaxLeftMotor, _MaxRightMotor, LINE_DELAY_DISTANCE/2, MOTOR_STOP_DELAY);
  threePointTurn(Left);
  travelStraight(_MaxLeftMotor, _MaxRightMotor, LINE_DELAY_DISTANCE/2, MOTOR_STOP_DELAY);
}

void drawH()
{  
  //For now dont worry about bounds. Perhaps add extra spacings between letters, and dont worry about in individual letters?
  //Turn left
  threePointTurn(Left);
  
  setPen(true);
  
  //Go to border
  motors.setSpeeds(_MaxLeftMotor, _MaxRightMotor);
  delay(LINE_DELAY_DISTANCE);
  motors.setSpeeds(0,0);
  
  setPen(false);
  
  //Go back half way
  motors.setSpeeds(-_MaxLeftMotor, -_MaxRightMotor);
  delay(LINE_DELAY_DISTANCE / 2);
  motors.setSpeeds(0,0);
  delay(MOTOR_STOP_DELAY);
  
  
  //Turn right
  threePointTurn(Right);
  
  setPen(true);
  
  //Go across
  motors.setSpeeds(_MaxLeftMotor, _MaxRightMotor);
  delay(LINE_DELAY_DISTANCE);
  motors.setSpeeds(0,0);
  
  setPen(false);
  
  //Turn left
  threePointTurn(Left);
  
  //go to upper right corner
  motors.setSpeeds(_MaxLeftMotor, _MaxRightMotor);
  delay(LINE_DELAY_DISTANCE/2);
  motors.setSpeeds(0,0);
  
  setPen(true);
  
  motors.setSpeeds(-_MaxLeftMotor, -_MaxRightMotor);
  delay(LINE_DELAY_DISTANCE);
  motors.setSpeeds(0,0);  
  
  setPen(false);
  
  //Turn right
  threePointTurn(Right);
}

void drawI()
{
  //Travel halfway through letter
  motors.setSpeeds(_MaxLeftMotor, _MaxRightMotor);
  delay(LINE_DELAY_DISTANCE / 2);
  motors.setSpeeds(0,0);
  delay(MOTOR_STOP_DELAY);
  
  //Turn left
  threePointTurn(Left);

  setPen(true);
  
  //Go to border
  motors.setSpeeds(_MaxLeftMotor, _MaxRightMotor);
  delay(LINE_DELAY_DISTANCE);
  motors.setSpeeds(0,0);
  delay(MOTOR_STOP_DELAY);
  
  setPen(false);
  
  //Go back
  motors.setSpeeds(-_MaxLeftMotor, -_MaxRightMotor);
  delay(LINE_DELAY_DISTANCE);
  motors.setSpeeds(0,0);
  delay(MOTOR_STOP_DELAY);
  
  //Turn right
  threePointTurn(Right);
  
  //Go to end of letter
  motors.setSpeeds(_MaxLeftMotor, _MaxRightMotor);
  delay(LINE_DELAY_DISTANCE /2);
  motors.setSpeeds(0,0);
  delay(MOTOR_STOP_DELAY);
}

void drawJ()
{
  travelStraight();
  threePointTurn(Left);
  travelStraight();
  threePointTurn(Left);
  threePointTurn(Left);
  setPen(true);
  travelStraight(_MaxLeftMotor, _MaxRightMotor, LINE_DELAY_DISTANCE/2, MOTOR_STOP_DELAY);
  drawCircle(2, Right, 2);
  setPen(false);
  threePointTurn(Left);
  threePointTurn(Left);
  travelStraight(_MaxLeftMotor, _MaxRightMotor, LINE_DELAY_DISTANCE/2, MOTOR_STOP_DELAY);
  threePointTurn(Left);
  travelStraight();
  return;
}

void drawK()
{
  //Implimented from untested drawV
  
  //Distance between upper corners and lower center. Borrowed from untested drawY. Fixed to be 8*4 instead of 8*8 since doing half distance
  double travelDelay = sqrt( pow( LINE_DELAY_DISTANCE/2, 2 ) + pow( LINE_DELAY_DISTANCE, 2 ) );
  
  threePointTurn(Left);
  setPen(true);
  travelStraight(_MaxLeftMotor, _MaxRightMotor, LINE_DELAY_DISTANCE, 0); //Set stop delay to 0 because changing pen state causes delay already
  setPen(false);
  travelStraight(-_MaxLeftMotor, -_MaxRightMotor, LINE_DELAY_DISTANCE/2, 0);
  threePointTurn(0.5, Right);
  setPen(true);
  travelStraight(_MaxLeftMotor, _MaxRightMotor, travelDelay, 0);
  setPen(false);
  travelStraight(-_MaxLeftMotor, -_MaxRightMotor, travelDelay,0);
  //threePointTurn(0.5, Right);
  //threePointTurn(0.5, Right);
  threePointTurn(Right);
  setPen(true);
  travelStraight(_MaxLeftMotor, _MaxRightMotor, travelDelay, 0);
  setPen(false);
  threePointTurn(0.5, Left);
  return;
  //REDUNDANT!
  travelStraight(-_MaxLeftMotor, -_MaxRightMotor, travelDelay,0);
  threePointTurn(0.5, Right);
  travelStraight(_MaxLeftMotor, _MaxRightMotor, LINE_DELAY_DISTANCE/2, 0);
  threePointTurn(Left);
  travelStraight(_MaxLeftMotor, _MaxRightMotor, LINE_DELAY_DISTANCE, 0);
  
}

void drawL()
{
  //Code taken from drawE();
  
  //For now dont worry about bounds. Perhaps add extra spacings between letters, and dont worry about in individual letters?
  //Turn left
  threePointTurn(Left);
  
  //Go to border
  motors.setSpeeds(_MaxLeftMotor, _MaxRightMotor);
  delay(LINE_DELAY_DISTANCE);
  motors.setSpeeds(0,0);
  
  setPen(true);
  
  //Go back
  motors.setSpeeds(-_MaxLeftMotor, -_MaxRightMotor);
  delay(LINE_DELAY_DISTANCE);
  motors.setSpeeds(0,0);
  
  setPen(false);
  
  //Turn right
  threePointTurn(Right);
  
  setPen(true);
  
  //go to other side
  motors.setSpeeds(_MaxLeftMotor, _MaxRightMotor);
  delay(LINE_DELAY_DISTANCE);
  motors.setSpeeds(0,0);
  
  setPen(false);  
}

void drawM()
{
  //Implimented from drawV
  //Distance between upper corners and lower center. Borrowed from untested drawY. Fixed to be 8*4 instead of 8*8
  double travelDelay = sqrt( pow( LINE_DELAY_DISTANCE/2, 2 ) + pow( LINE_DELAY_DISTANCE, 2 ) );
  
  threePointTurn(Left);
  setPen(true);
  travelStraight();
  setPen(false);
  threePointTurn(1.5 /*1.25*/, Right); //Turn to face center bottom
  setPen(true);
  travelStraight(_MaxLeftMotor, _MaxRightMotor, travelDelay, 0); //Set stop delay to 0 because changing pen causes delay already
  setPen(false);
  threePointTurn(Left);
  setPen(true);
  travelStraight(_MaxLeftMotor, _MaxRightMotor, travelDelay,0);
  setPen(false);
  threePointTurn(/*1.25*/1.5, Right);
  setPen(true);
  travelStraight();
  setPen(false);
  threePointTurn(Left);
  
  return;
}

void drawN()
{  
  //Complete rewrite
  
  //Turn right
  threePointTurn(Right);
  
  setPen(true);
  
  //go to end, in reverse
  motors.setSpeeds(-_MaxLeftMotor, -_MaxRightMotor);
  delay(LINE_DELAY_DISTANCE);
  motors.setSpeeds(0,0);
  delay(MOTOR_STOP_DELAY);
  
  setPen(false);
  
  //Turn 45 degrees left
  threePointTurn(0.5, Left);
  
  setPen(true);
  
  Serial.print("Hypotinuse is: ");
  Serial.println(sqrt( pow( LINE_DELAY_DISTANCE, 2 ) + pow( LINE_DELAY_DISTANCE, 2 ) ));
  double hypotinuse = sqrt( pow( LINE_DELAY_DISTANCE, 2 ) + pow( LINE_DELAY_DISTANCE, 2 ) );
  //Go across. 
  motors.setSpeeds(_MaxLeftMotor, _MaxRightMotor);
  //We need to travel the hypotinus diagonal from upper left to lower right
  delay( hypotinuse);
  motors.setSpeeds(0,0);
  delay(MOTOR_STOP_DELAY);
  
  setPen(false);
  
  //Turn back up, another 45 degrees
  threePointTurn(0.5, Right);
  
  setPen(true);
  
  //Go up
  motors.setSpeeds(-_MaxLeftMotor, -_MaxRightMotor);
  delay(LINE_DELAY_DISTANCE);
  motors.setSpeeds(0,0);
  
  setPen(false);
  
  //Go back to corner
  motors.setSpeeds(_MaxLeftMotor, _MaxRightMotor);
  delay(LINE_DELAY_DISTANCE);
  motors.setSpeeds(0,0);
  delay(MOTOR_STOP_DELAY);
  
  //Turn left
  threePointTurn(Left);
}

void drawO()
{
  //PLACEHOLDER! MIGHT NEED REWRITE
  travelStraight(_MaxLeftMotor, _MaxRightMotor, LINE_DELAY_DISTANCE/2, 0);
  setPen(true);
  drawCircle();
  setPen(false);
  travelStraight(_MaxLeftMotor, _MaxRightMotor, LINE_DELAY_DISTANCE/2, 0);
}

void drawP()
{
  //Untested
  threePointTurn(Left);
  setPen(true);
  travelStraight();
  setPen(false);
  threePointTurn(Right);
  setPen(true);
  drawCircle(2, Right, 2);
  setPen(false);
  threePointTurn(Left);
  travelStraight(_MaxLeftMotor, _MaxRightMotor, LINE_DELAY_DISTANCE/2, MOTOR_STOP_DELAY);
  threePointTurn(Left);
  travelStraight();
}

void drawQ()
{
  //based on untested drawO
  travelStraight(_MaxLeftMotor, _MaxRightMotor, LINE_DELAY_DISTANCE/2, 0);
  setPen(true);
  drawCircle();
  setPen(false);
  drawCircle(8, Left);
  threePointTurn(Left);
  setPen(true);
  travelStraight(-_MaxLeftMotor, -_MaxRightMotor, LINE_DELAY_DISTANCE/9, 0); //Untested value
  setPen(false);
  travelStraight(_MaxLeftMotor, _MaxRightMotor, LINE_DELAY_DISTANCE/9, 0); //Untested value
  threePointTurn(Left);
  drawCircle(8, Right);
  //Turn 180
  threePointTurn(Left);
  threePointTurn(Left);
  
  travelStraight(_MaxLeftMotor, _MaxRightMotor, LINE_DELAY_DISTANCE/2, 0);
}

void drawR()
{
  //UNTESTED
  double travelDelay = sqrt( pow( LINE_DELAY_DISTANCE/2, 2 ) + pow( LINE_DELAY_DISTANCE, 2 ) ) / 2;
  
  threePointTurn(Left);
  setPen(true);
  travelStraight();
  setPen(false);
  threePointTurn(Right);
  setPen(true);
  drawCircle(2, Right, 2);
  setPen(false);
  threePointTurn(Right); // Turn 180
  threePointTurn(Right);
  threePointTurn(0.25, Right);
  setPen(true);
  travelStraight(_MaxLeftMotor, _MaxRightMotor, travelDelay, 0);
  setPen(false);
  threePointTurn(0.25, Left);  
}

void drawS()
{
  //UNTESTED
  travelStraight(_MaxLeftMotor, _MaxRightMotor, LINE_DELAY_DISTANCE/4, 0);
  setPen(true);
  drawCircle(2, Left, 2);
  drawCircle(2, Right, 2);
  setPen(false);
  threePointTurn(Right);
  travelStraight();
  threePointTurn(Left);
  travelStraight(_MaxLeftMotor, _MaxRightMotor, LINE_DELAY_DISTANCE/4, MOTOR_STOP_DELAY);
}

void drawT()
{
  //Adapted from drawI();
  
  //Travel halfway through letter
  motors.setSpeeds(_MaxLeftMotor, _MaxRightMotor);
  delay(LINE_DELAY_DISTANCE / 2);
  motors.setSpeeds(0,0);
  delay(MOTOR_STOP_DELAY);
  
  //Turn left
  threePointTurn(Left);
  
  setPen(true);
  
  //Go to border
  motors.setSpeeds(_MaxLeftMotor, _MaxRightMotor);
  delay(LINE_DELAY_DISTANCE);
  motors.setSpeeds(0,0);
  delay(MOTOR_STOP_DELAY);
  
  setPen(false);
  
  //Turn left
  threePointTurn(Left);
  
  //Travel halfway through letter, to upper left
  motors.setSpeeds(_MaxLeftMotor, _MaxRightMotor);
  delay(LINE_DELAY_DISTANCE / 2);
  motors.setSpeeds(0,0);
  
  setPen(true);
  
  //Travel across letter, to upper right, backwards
  motors.setSpeeds(-_MaxLeftMotor, -_MaxRightMotor);
  delay(LINE_DELAY_DISTANCE);
  motors.setSpeeds(0,0);
  
  setPen(false);
  
  //Turn left, to face down
  threePointTurn(Left);
  
  //Go down
  motors.setSpeeds(_MaxLeftMotor, _MaxRightMotor);
  delay(LINE_DELAY_DISTANCE);
  motors.setSpeeds(0,0);
  
  threePointTurn(Left);
}

void drawU()
{
  //Untested
  threePointTurn(Left);
  travelStraight();
  threePointTurn(Left); //Turn 180
  threePointTurn(Left);
  setPen(true);
  travelStraight(_MaxLeftMotor, _MaxRightMotor, LINE_DELAY_DISTANCE/2, MOTOR_STOP_DELAY);
  drawCircle(2, Left);
  travelStraight(_MaxLeftMotor, _MaxRightMotor, LINE_DELAY_DISTANCE/2, MOTOR_STOP_DELAY);
  setPen(false);
  threePointTurn(Left); //Turn 180
  threePointTurn(Left);
  travelStraight();
  threePointTurn(Left);
}

void drawV()
{
  //Distance between upper corners and lower center. Borrowed from untested drawY. Fixed to be 8*4 instead of 8*8
  double travelDelay = sqrt( pow( LINE_DELAY_DISTANCE/2, 2 ) + pow( LINE_DELAY_DISTANCE, 2 ) );
  
  threePointTurn(Left);
  travelStraight();
  threePointTurn(1.5 /*1.25*/, Right); //Turn to face center bottom
  setPen(true);
  travelStraight(_MaxLeftMotor, _MaxRightMotor, travelDelay, 0); //Set stop delay to 0 because changing pen causes delay already
  setPen(false);
  threePointTurn(Left);
  setPen(true);
  travelStraight(_MaxLeftMotor, _MaxRightMotor, travelDelay,0);
  setPen(false);
  threePointTurn(/*1.25*/1.5, Right);
  travelStraight();
  threePointTurn(Left);
}

void drawW()
{
  //draw in terms of A
  threePointTurn(Left);
  setPen(true);
  travelStraight();
  setPen(false);
  travelStraight(-_MaxLeftMotor, -_MaxRightMotor, LINE_DELAY_DISTANCE, MOTOR_STOP_DELAY);
  threePointTurn(Right);
  
  //A code
  //Complete Rewrite. Now actually works, aside from the A ridge not being right.
  //Distance between upper corners and lower center. Borrowed from untested drawY. Fixed to be 8*4 instead of 8*8
  
  double travelDelay = sqrt( pow( LINE_DELAY_DISTANCE/2, 2 ) + pow( LINE_DELAY_DISTANCE, 2 ) );
  threePointTurn(0.5, Left);
  setPen(true);
  travelStraight(_MaxLeftMotor, _MaxRightMotor, travelDelay, 0);
  setPen(false);
  threePointTurn(0.5, Right);
  threePointTurn(0.5, Right);
  setPen(true);
  travelStraight(_MaxLeftMotor, _MaxRightMotor, travelDelay, 0);
  setPen(false);
  
  //Turn and go to end.
  threePointTurn(0.5, Left);
  
  //end of recycled A code
  
  threePointTurn(Left);
  setPen(true);
  travelStraight();
  setPen(false);
  travelStraight(-_MaxLeftMotor, -_MaxRightMotor, LINE_DELAY_DISTANCE, MOTOR_STOP_DELAY);
  threePointTurn(Right);
  return;
}

void drawX()
{
  //Complete rewrite
  
  //Turn right
  threePointTurn(Right);
  
  //go to end, in reverse
  motors.setSpeeds(-_MaxLeftMotor, -_MaxRightMotor);
  delay(LINE_DELAY_DISTANCE);
  motors.setSpeeds(0,0);
  delay(MOTOR_STOP_DELAY);
  
  setPen(false);
  
  //Turn 45 degrees left
  threePointTurn(0.5, Left);
  
  setPen(true);
  
  Serial.print("Hypotinuse is: ");
  Serial.println(sqrt( pow( LINE_DELAY_DISTANCE, 2 ) + pow( LINE_DELAY_DISTANCE, 2 ) ));
  double hypotinuse = sqrt( pow( LINE_DELAY_DISTANCE, 2 ) + pow( LINE_DELAY_DISTANCE, 2 ) );
  //Go across. 
  motors.setSpeeds(_MaxLeftMotor, _MaxRightMotor);
  //We need to travel the hypotinus diagonal from upper left to lower right
  delay( hypotinuse);
  motors.setSpeeds(0,0);
  delay(MOTOR_STOP_DELAY);
  
  setPen(false);
  
  //Turn back up, another 45 degrees
  threePointTurn(0.5, Right);
  
  //Go up
  motors.setSpeeds(-_MaxLeftMotor, -_MaxRightMotor);
  delay(LINE_DELAY_DISTANCE);
  motors.setSpeeds(0,0);
  delay(MOTOR_STOP_DELAY);
  
  //Turn 45 degrees right
  threePointTurn(0.5, Right);
  
  setPen(true);
  
  //Go across. 
  motors.setSpeeds(_MaxLeftMotor, _MaxRightMotor);
  //We need to travel the hypotinus diagonal from upper left to lower right
  delay( hypotinuse);
  motors.setSpeeds(0,0);
  
  setPen(false);
  
  threePointTurn(0.5, Left);
  threePointTurn(Left);
  
  drawSpace();
}

void drawY()
{
  double travelDelay = sqrt( pow( LINE_DELAY_DISTANCE/2, 2 ) + pow( LINE_DELAY_DISTANCE, 2 ) ) / 2;
  
  //Travel to middle of letter
  travelStraight(_MaxLeftMotor,_MaxRightMotor, LINE_DELAY_DISTANCE/2, MOTOR_STOP_DELAY);
  threePointTurn(Left);
  setPen(true);
  travelStraight(_MaxLeftMotor,_MaxRightMotor, LINE_DELAY_DISTANCE/2, 0);
  setPen(false);
  threePointTurn(0.5, Left);
  setPen(true);
  travelStraight(_MaxLeftMotor, _MaxRightMotor, travelDelay, 0);
  setPen(false);
  travelStraight(-_MaxLeftMotor, -_MaxRightMotor, travelDelay, 0);
  threePointTurn(Right);
  setPen(true);
  travelStraight(_MaxLeftMotor, _MaxRightMotor, travelDelay, 0);
  setPen(false);
  travelStraight(-_MaxLeftMotor, -_MaxRightMotor, travelDelay, 0);
  threePointTurn(0.5, Left);
  travelStraight(-_MaxLeftMotor,-_MaxRightMotor, LINE_DELAY_DISTANCE/2, MOTOR_STOP_DELAY);
  threePointTurn(Right);
  travelStraight(_MaxLeftMotor,_MaxRightMotor, LINE_DELAY_DISTANCE/2, MOTOR_STOP_DELAY);  
}

void drawZ()
{
  Serial.print("Hypotinuse is: ");
  Serial.println(sqrt( pow( LINE_DELAY_DISTANCE, 2 ) + pow( LINE_DELAY_DISTANCE, 2 ) ));
  double hypotinuse = sqrt( pow( LINE_DELAY_DISTANCE, 2 ) + pow( LINE_DELAY_DISTANCE, 2 ) );
  
  threePointTurn(Left);
  travelStraight();
  threePointTurn(Right);
  setPen(true);
  travelStraight();
  setPen(false);
  threePointTurn(Right);
  threePointTurn(0.5, Right);
  setPen(true);
  travelStraight(_MaxLeftMotor, _MaxRightMotor, hypotinuse, 0);
  setPen(false);
  threePointTurn(0.5, Left);
  threePointTurn(Left);
  setPen(true);
  travelStraight();
  setPen(false);
  return;
}

void drawBox()
{
  //Turn left
  threePointTurn(Left);
  
  //Removed delay because pen will cause delay
  setPen(true);
  
  //Go to border, draw left side
  motors.setSpeeds(_MaxLeftMotor, _MaxRightMotor);
  delay(LINE_DELAY_DISTANCE);
  motors.setSpeeds(0,0);
  delay(100);
  
  setPen(false);
  
  //Turn right.
  threePointTurn(Right);
  
  setPen(true);
  
  //Go to border, draw top
  motors.setSpeeds(_MaxLeftMotor, _MaxRightMotor);
  delay(LINE_DELAY_DISTANCE);
  motors.setSpeeds(0,0);
  delay(100);
  
  setPen(false);
  
  //Turn right
  threePointTurn(Right);
  
  setPen(true);
  
  //Go to border, draw right side
  motors.setSpeeds(_MaxLeftMotor, _MaxRightMotor);
  delay(LINE_DELAY_DISTANCE);
  motors.setSpeeds(0,0);
  delay(100);
  
  setPen(false);
  
  //Turn left
  threePointTurn(Left);
  
  setPen(true);
  
  //Draw bottom of box.
  motors.setSpeeds(-_MaxLeftMotor, -_MaxRightMotor);
  delay(LINE_DELAY_DISTANCE);
  motors.setSpeeds(0,0);
  delay(100);  
  
  //Go to end.
  drawSpace();
}

void drawSpace()
{
  //Lift pen if not already up
  setPen(false);
  
  motors.setSpeeds(_MaxLeftMotor, _MaxRightMotor);
  
  //Assuming _LetterSize is delay value in motor time
  delay(LINE_DELAY_DISTANCE);
  motors.setSpeeds(0,0);
  delay(200);
}

void drawBackspace()
{
  //Lift pen if not already up
  setPen(false);
  
  motors.setSpeeds(-_MaxLeftMotor, -_MaxRightMotor);
  
  //Assuming _LetterSize is delay value in motor time
  delay(LINE_DELAY_DISTANCE);
  motors.setSpeeds(0,0);
  delay(200);
  
  _LineDistance--;
}

void newLine()
{
  //Lift pen if not already up
  setPen(false);
  
  for(; _LineDistance > 0;)
    drawBackspace();
  
  //Turn 90 degress to face down.
  threePointTurn(Right);
  
  drawSpace();
  
  //Turn -90 degress to face right again.
  threePointTurn(Left);
  
  _CurrentLine++;
  
}

//Parse a given string and call appropriate letter funcs.
void drawString(char* strToDraw)
{
  int length = strlen(strToDraw);
  for(int index = 0; index < length; index++)
  {    
    //Converts any lower case letters to upper case, assuming ASCII values.
    //65-90 are A-Z, 97-122 are a-z. \n and space are unaffected (10 and 32)
    if(strToDraw[index] > 90)
      strToDraw[index] -= 32;
      
    switch (strToDraw[index])
    {
      case 'A':
        drawA();
        break;
      case 'B':
        drawB();
        break;
      case 'C':
        drawC();
        break;
      case 'D':
        drawD();
        break;
      case 'E':
        drawE();
        break;
      case 'F':
        drawF();
        break;
      case 'G':
        drawG();
        break;
      case 'H':
        drawH();
        break;
      case 'I':
        drawI();
        break;
      case 'J':
        drawJ();
        break;
      case 'K':
        drawK();
        break;
      case 'L':
        drawL();
        break;
      case 'M':
        drawM();
        break;
      case 'N':
        drawN();
        break;
      case 'O':
        drawO();
        break;
      case 'P':
        drawP();
        break;
      case 'Q':
        drawQ();
        break;
      case 'R':
        drawR();
        break;
      case 'S':
        drawS();
        break;
      case 'T':
        drawT();
        break;
      case 'U':
        drawU();
        break;
      case 'V':
        drawV();
        break;
      case 'W':
        drawW();
        break;
      case 'X':
        drawX();
        break;
      case 'Y':
        drawY();
        break;
      case 'Z':
        drawY();
        break;
      case '=': //box
        drawBox();
        break;
      case ' ':
        drawSpace();
        break;
      case '\b':
        drawBackspace();
        break;
      case '\n':
        newLine();
        break;
      default:
        drawSpace();
    }
    
    //Incriment line distance, unless backspace or new line. 
    if(strToDraw[index] != '\b' && strToDraw[index] != '\n')
      _LineDistance++;
    
    delay(200);
  }
}

//Elipse code. Adapted from Nathan's work over the break.


#define _USE_MATH_DEFINES

#define motor1 0

#define motor2 1

#define LEFT 1

#define RIGHT 0

void drawCircle()
{
  drawCircle(1, Left, 4); //Will draw a 9" circle
}

void drawCircle(int fraction, int dir)
{
  drawCircle(fraction, dir, 4);
}

void drawCircle (int fraction, int dir, double radius)
{
  //find motor values to draw a fraction of a circle

    //With wheel size at 2.37, this is drawing a 9" circle
    //As such, propose bumping letter size up to 9" to make things easier.
    int a=0,//motor1 speed
    b=0,//motor2 speed
    f= fraction,//2,//what fraction of a circle you want 4= quarter circle, 2= half circle //2 is a fourth? 4 is eighth
    turn=dir; //which direction robot turns

    double max[2]={_MaxLeftMotor, _MaxRightMotor};//motors top speed **not yet known**

    double distanceG,//distance greater wheel travels
     distanceL,//distance lesser wheel travels
     //radius = 4,//length of radius
     wheel=1.9, //orig 2.35//length to wheel
     motorSpeed = 92.5,
     time;//how long motors will run
     
    distanceG=(radius+wheel)*M_PI*2/f;//distance greater wheel travels

    distanceL=(radius-wheel)*M_PI*2/f;//distance lesser wheel travels

    time= distanceG * motorSpeed; //92.5 derived from 8" delay distance div 8 //max[turn] * 1000;//how long motors will run
    
    double relationship = distanceG / distanceL;
    
    if(turn==Left)
    {
        b=max[motor2];
        //a= (time / distanceL) * max[motor1];
        a = ((distanceL * _MaxLeftMotor) + (distanceG * relationship))/distanceG;
        //200 - (200 / motorDiff); //Max speed right motor
    }
    else if(turn==Right)
    {
        a=max[motor1];
        //b=distanceL/time*max[motor2];
        b = ((distanceL * _MaxRightMotor) + (distanceG * relationship))/distanceG;
    }
    else
    {
        a=max[motor1];
        b=max[motor2];
    }
    
    motors.setSpeeds(a,b);
    delay(time * 2); //Times 2 as ugly hack to make it draw proper.
    motors.setSpeeds(0,0);

}

void drawCircle2()
{
    //find motor values to draw a fraction of a circle

    //With wheel size at 2.37, this is drawing a 9" circle
    //As such, propose bumping letter size up to 9" to make things easier.
    int a=0,//motor1 speed
    b=0,//motor2 speed
    f=1,//2,//what fraction of a circle you want 4= quarter circle, 2= half circle //2 is a fourth? 4 is eighth
    turn=LEFT; //which direction robot turns

    double max[2]={_MaxLeftMotor, _MaxRightMotor};//motors top speed **not yet known**

    double distanceG,//distance greater wheel travels
     distanceL,//distance lesser wheel travels
     radius=4,//length of radius
     wheel=2.37,//length to wheel
     motorSpeed = 92.5,
     time;//how long motors will run
     
    distanceG=(radius+wheel)*M_PI*2/f;//distance greater wheel travels

    distanceL=(radius-wheel)*M_PI*2/f;//distance lesser wheel travels

    time= distanceG * motorSpeed; //92.5 derived from 8" delay distance div 8 //max[turn] * 1000;//how long motors will run
    
    double relationship = distanceG / distanceL;
    
    if(turn==LEFT)
    {
        b=max[motor2];
        //a= (time / distanceL) * max[motor1];
        a = ((distanceL * _MaxLeftMotor) + (distanceG * relationship))/distanceG;
        //200 - (200 / motorDiff); //Max speed right motor
    }
    else if(turn==RIGHT)
    {
        a=max[motor1];
        //b=distanceL/time*max[motor2];
        b = ((distanceL * _MaxRightMotor) + (distanceG * relationship))/distanceG;
    }
    else
    {
        a=max[motor1];
        b=max[motor2];
    }
    
    motors.setSpeeds(a,b);
    delay(time * 2); //Times 2 as ugly hack to make it draw proper.
    motors.setSpeeds(0,0);

}


