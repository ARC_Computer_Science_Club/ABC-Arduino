#include <ZumoMotors.h>
#include <Pushbutton.h>

#define MAX_LEFT_MOTOR_BENDER 400
#define MAX_RIGHT_MOTOR_BENDER 325
#define MAX_LEFT_MOTOR_JOHNNY 400
#define MAX_RIGHT_MOTOR_JOHNNY 327
ZumoMotors motors;
Pushbutton button(ZUMO_BUTTON);

bool bFirstRun = true;
void setup()
{
}
void loop()
{
  if(bFirstRun)
  {
    //while(!button.isPressed());
    bFirstRun = false;
    delay(2000);
  }
  motors.setLeftSpeed(MAX_LEFT_MOTOR_JOHNNY);
  motors.setRightSpeed(MAX_RIGHT_MOTOR_JOHNNY);
  delay (210);
  motors.setLeftSpeed(0);
  motors.setRightSpeed(0);
  delay (500);
  motors.setLeftSpeed(MAX_LEFT_MOTOR_JOHNNY);
  motors.setRightSpeed(-330);
  delay (240);
  motors.setLeftSpeed(0);
  motors.setRightSpeed(0);
  delay (2000);
 
}
  
