#include <ZumoMotors.h>
#include <Pushbutton.h>
#include <ZumoBuzzer.h>
//#include <Servo.h>
#include <stdlib.h>

//Tried values of 4, 5 and 11. Issue occured on all three.
//#define SERVO_PIN 5

Pushbutton button(ZUMO_BUTTON);
ZumoMotors motors;
ZumoBuzzer buzzer;
//Servo penservo;

bool _FirstRun = true;

void setup()
{
  //With this line, the motor acts up when value is changed.
  //When commented out, code runs fine.
  //penservo.attach (SERVO_PIN);
  servoInit();
}

void loop()
{
  if(_FirstRun)
  {    
    /*
    buzzer.playNote(NOTE_A(1), 168, 15);
    */
    while (!button.isPressed());
    _FirstRun = false;
  }   
  
  buzzer.playNote(NOTE_B(5), 168, 15);  
  
  motors.setSpeeds(400,400);
  servoSetPosition(1000);  // Send 1000us pulses.
  delay(1000); 
  motors.setSpeeds(0,0);
  servoSetPosition(2000);  // Send 2000us pulses.
  delay(1000);
  //This line should do nothing.
  //When the line in setup() is not commented, right motor activates here.
  //motors.setSpeeds(0,0);
  
  //The right motor will also activate with either of the following lines
  //motors.setLeftSpeed(0);
  //or
  //motors.setRightSpeed(0);
  /*
  buzzer.playNote(NOTE_C(5), 168, 15);
  */
  
  //When finished, wait for button press to go again. 
  while(!button.isPressed());
}
